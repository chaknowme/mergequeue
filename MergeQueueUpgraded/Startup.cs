﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MergeQueueUpgraded.Startup))]
namespace MergeQueueUpgraded
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
