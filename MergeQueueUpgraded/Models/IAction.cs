﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeQueueUpgraded.Models
{
    public enum ActionSource
    {
        System,
        User
    }
    public enum ActionType
    {
        AttemptToLogin,
        CreateMergeRequest
    }
    public enum ActionStatus
    {
        None,
        Success,
        Failure,
        Interrupted,
        Suspended
    }
    public interface IActionParameter
    {
        string Name
        {
            get;
            set;
        }
        string Description
        {
            get;
            set;
        }
    }
    delegate ActionStatus ActionDelegate(params IActionParameter[] args);
    public interface IAction
    {
        ActionSource Source
        {
            get;
            set;
        }

        ActionType Action
        {
            get;
            set;
        }

        IActionParameter Perform
        {
            get;
            set;
        }

        string Definition
        {
            get;
            set;
        }

        string CustomInformation
        {
            get;
            set;
        }
    }
}
