﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Reflection;
using log4net;

namespace MergeQueueUpgraded.Models.Logging
{
    public class MergeQueueLogger
    {
        private static readonly MergeQueueLogger m_logger = new MergeQueueLogger();
        private MergeQueueLogger()
        {

        }
        public static MergeQueueLogger Instance
        {
            get
            {
                return m_logger;
            }
        }

        public void LogAction(IAction action, ActionStatus result, List<IActionParameter> parameters)
        {
            string parametersToString = "";
            foreach(var parm in parameters)
            {
                parametersToString += string.Format(",{Name: {0}, Description: {1}}",parm.Name,parm.Description);
            }
            parametersToString = "(" + parametersToString.Substring(1) + ")";

            string msg = string.Format("Action: {0} - {1}, Source: {2}, Params: {3}, Result: {4}, Custom: {5}", 
                action.Action, action.Definition, action.Source, parametersToString, result, action.CustomInformation);

            StackTrace trace = new StackTrace();
            LogMessageInternal(LogManager.GetLogger(typeof(MergeQueueLogger)).Info, msg, trace.GetFrame(1).GetMethod());
        }

        public void LogInfoMessage(string msg)
        {
            StackTrace trace = new StackTrace();
            LogMessageInternal(LogManager.GetLogger(typeof(MergeQueueLogger)).Info, msg, trace.GetFrame(1).GetMethod());
        }
        private delegate void LoggerFunc(object message);

        private void LogMessageInternal(LoggerFunc logFunc,string msg, MethodBase callingMethod)
        {
            logFunc(string.Format("{0}.{1}: {2}", callingMethod.DeclaringType.Name, callingMethod.Name, msg));
        }

    }
}